require 'bunny'

class AMQP
  def initialize(host)
    @conn = Bunny.new(host)

    @conn.start
  end

  def channel
    @channel ||= conn.create_channel
  end

  def publish(*args)
    exchange.publish(*args)
  end

  private

  def exchange
    @exchange ||= channel.topic("/messages", auto_delete: false)
  end

  attr_reader :conn
end
