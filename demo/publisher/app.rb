require 'sinatra'
require './lib/amqp'

STDOUT.sync = true

set :bind, '0.0.0.0'
set :port, 5000

amqp = AMQP.new('amqp://guest:guest@rabbit:5672')

get '/' do
  erb :index
end

post '/message' do
  amqp.publish(params[:message], routing_key: 'messages')

  redirect('/')
end
