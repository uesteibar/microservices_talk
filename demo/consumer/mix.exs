defmodule Consumer.MixProject do
  use Mix.Project

  def project do
    [
      app: :consumer,
      version: "0.1.0",
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  def application do
    [
      extra_applications: [:logger, :errol],
      mod: {Consumer.Application, []}
    ]
  end

  defp deps do
    [
      {:errol, git: "https://github.com/uesteibar/errol.git"}
    ]
  end
end
