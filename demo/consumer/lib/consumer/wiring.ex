defmodule Consumer.Wiring do
  use Errol.Wiring

  @connection [host: "rabbit"]
  @exchange "/messages"
  @exchange_type :topic

  consume("messages", "messages", fn %{payload: payload} = message ->
    IO.puts("""
    ###########################
    #{payload}
    """)

    message
  end)
end
